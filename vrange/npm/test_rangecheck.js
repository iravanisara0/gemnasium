#!/usr/bin/env node

// run with ./test_rangecheck.js

function clean_string(str) {
    return str.replace(/[^a-zA-Z0-9]/g, "")
}

var testing = require('testing');
var fs = require("fs");

function testWellFormedJsonFile(callback) {
    var expect = String(fs.readFileSync('tests/simple_out.json'));
    var spawn = require('child_process').spawnSync;
    var command = spawn('./rangecheck.js', ['tests/simple_in.json']);
    var out = String(command.stdout);
    var status = command.status;
    expect = clean_string(expect);
    out = clean_string(out);
    testing.assertEquals(status, 0);
    testing.assertEquals(out, expect);
    testing.success(callback);
}

function testDictionaryJsonFile(callback) {
    var spawn = require('child_process').spawnSync;
    const command = spawn('./rangecheck.js', ['tests/dictionary.json']);
    var out = String(command.stderr);
    testing.assertEquals(out, "Unexpected token { in JSON at position 2\n");
    testing.assertEquals(command.status, 1);
    testing.success(callback);
}

function testEmptyJsonFile(callback) {
    var spawn = require('child_process').spawnSync;
    const command = spawn('./rangecheck.js', ['tests/empty.json']);
    testing.assertEquals(String(command.stderr), "File input is not a JSON array.\n");
    testing.assertEquals(command.status, 1);
    testing.success(callback);
}

function testNonExistentJsonFile(callback) {
    var spawn = require('child_process').spawnSync;
    const command = spawn('./rangecheck.js', ['tests/empty00.json']);
    testing.assertEquals(String(command.stderr), "ENOENT: no such file or directory, open 'tests/empty00.json'\n");
    testing.assertEquals(command.status, 1);
    testing.success(callback);
}


function testGemnasiumDbAdvisoryRanges(callback) {
    var expect = String(fs.readFileSync('tests/adb_ranges_out.json'));
    var spawn = require('child_process').spawnSync;
    var command = spawn('./rangecheck.js', ['tests/adb_ranges_in.json']);
    var out = String(command.stdout);
    var status = command.status
    expect = clean_string(expect);
    out = clean_string(out)
    testing.assertEquals(status, 0);
    testing.assertEquals(out, expect);
    testing.success(callback);
}

exports.test = function(callback)
{
    var tests = [
        testWellFormedJsonFile,
        testDictionaryJsonFile,
        testEmptyJsonFile,
        testNonExistentJsonFile,
        testGemnasiumDbAdvisoryRanges
    ];
    testing.run(tests, callback);
};

// run tests if invoked directly
if (__filename == process.argv[1])
{
    exports.test(testing.show);
}
