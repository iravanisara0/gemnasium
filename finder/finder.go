package finder

import (
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

// SearchMode defines what should be skipped after there is match
type SearchMode int

const (
	// SearchAll is when the finder search for all compatible projects
	SearchAll SearchMode = iota

	// SearchSingleDir is when the finder skips other directories after there was a match
	SearchSingleDir

	// SearchSingleTree is when the finder skips other directory trees after there was a match
	SearchSingleTree
)

// NullInt is an integer that may not be defined.
type NullInt struct {
	Valid bool // Valid is set when the integer is defined
	Value int  // Value is the integer value
}

// DetectFunc is the type of a function that detects projects in a set of files
type DetectFunc func(filenames []string) []Project

// Finder finds supported projects
type Finder struct {
	Detect           DetectFunc // Detect is the function used to detect projects in a directory
	FileTypes        []FileType // FileTypes are the file types to be detected
	SearchMode       SearchMode // SearchMode defines what should be skipped after there is match
	MaxDepth         NullInt    // MaxDepth is the maximum depth of directories being searched
	IgnoredDirs      []string   // IgnoredDirs are directories to be ignored when searching for projects
	IgnoreHiddenDirs bool       // IgnoreHiddenDirs is set to ignore directories whose name starts with ".".
}

// FindProjects walks through a directory tree and search for compatible projects.
func (f Finder) FindProjects(root string) ([]Project, error) {
	return f.findProjects(root, ".", f.SearchMode, 0)
}

func (f Finder) findProjects(root, dir string, mode SearchMode, depth int) ([]Project, error) {
	log.Debugf("scanning directory: %s", dir)

	// read directory
	file, err := os.Open(filepath.Join(root, dir))
	if err != nil {
		return nil, err
	}
	infos, err := file.Readdir(-1)
	// close the directory right after reading it
	// to avoid opening its sub-directories while it's still open
	if closeErr := file.Close(); closeErr != nil {
		return nil, closeErr
	}
	if err != nil {
		return nil, err
	}

	// collect file and directory names
	dirnames := []string{}
	filenames := []string{}
	for _, info := range infos {
		// TODO skip path if excluded via DS_EXCLUDED_PATHS (pre-filter)
		// See https://gitlab.com/gitlab-org/gitlab/-/issues/292457

		// add file or directory name
		if info.IsDir() {
			// skip directory if ignored
			path := filepath.Join(dir, info.Name())
			if f.dirIsIgnored(info.Name()) {
				log.Debugf("skip ignored directory: %s", path)
				continue
			}
			dirnames = append(dirnames, info.Name())
		} else {
			filenames = append(filenames, info.Name())
		}
	}

	// detect projects and set directory
	projects := f.Detect(filenames)
	for i := range f.Detect(filenames) {
		projects[i].Dir = dir
	}

	if len(projects) > 0 {
		switch mode {
		case SearchSingleDir:
			// stop searching
			log.Debug("skipping sub-directories")
			return projects, nil
		case SearchSingleTree:
			// search the entire sub-tree
			log.Debug("searching sub-tree")
			mode = SearchAll
		}
	}

	// skip directories if their depth is greater than maximum depth
	if f.MaxDepth.Valid && depth+1 > f.MaxDepth.Value {
		return projects, nil
	}

	// process directories
	for _, dirname := range dirnames {
		subdir := filepath.Join(dir, dirname)
		subprojects, err := f.findProjects(root, subdir, mode, depth+1)
		if err != nil {
			return nil, err
		}
		projects = append(projects, subprojects...)

		// stop if there was a match unless searching for all
		if mode != SearchAll && len(subprojects) > 0 {
			log.Debug("skipping other trees")
			break
		}
	}

	return projects, err
}

func (f Finder) dirIsIgnored(dir string) bool {
	if f.IgnoreHiddenDirs && strings.HasPrefix(dir, ".") {
		return true
	}
	for _, ignored := range f.IgnoredDirs {
		if dir == ignored {
			return true
		}
	}
	return false
}
