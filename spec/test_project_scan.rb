require 'json'

# TestProjectScan is used to scan a test project using the analyzer Docker image,
# and to collect the generated report as well as the analyzer exit code.
#
# Before the scan, it clones the test project, checkouts the tested branch,
# and restores that branch to its original state.
#
# It depends on the docker CLI to perform the scan,
# and on the git CLI to clone the test project.
#
class TestProjectScan

  # This class is configured using class attributes.
  class << self
    attr_accessor :image_name        # name of the Docker image being tested
    attr_accessor :fixtures_dir      # directory where the test projects are cloned
    attr_accessor :test_projects_url # base URL of the test projects
    attr_accessor :global_vars       # environment variables to be set when running the Docker image
    attr_accessor :report_filename   # filename of the generated report, like gl-dependency-scanning-report.json
  end

  attr_reader :project_name, :branch, :variables, :mutate_project

  # Initialize a scan of a test project with a given name.
  #
  # branch is the name of the test branch.
  #
  # variables is a hash that defines environment variables to be set when running the image.
  # These variables are merged into the ones defined in global_vars.
  #
  # mutate_project is a proc that takes the path where the test project is cloned,
  # and that alters the project files. The files must be altered using git commands
  # so that all changes can be reverted using git reset.
  #
  # The class must be configured using class attributes before calling initialize.
  #
  def initialize(project_name, branch: "master", variables: nil, mutate_project: nil)
    @project_name = project_name
    @branch = branch
    @variables = variables.nil? ? Hash.new : variables
    @mutate_project = mutate_project
  end

  # Prepare the test project, run the analyzer, and return scan results.
  # It returns the report path, the report itself (decoded from JSON, if the file exists),
  # and the exit code of the Docker container.
  #
  # The test project is fetched using git clone if needed.
  # It's then reset to the requested branch using git reset --hard,
  # so that all changes made using git commands are reverted.
  # It then mutates the project by calling the mutate_project proc, if defined.
  #
  # Reports created by previous executions of the analyzer are removed before the scan.
  #
  def run
    fetch_project
    reset_branch
    remove_report
    mutate_project.call(project_dir) unless mutate_project.nil?
    exit_code = docker_run
    return report_path, report, exit_code
  end

  private

  def image_name
    self.class.image_name
  end

  def fixtures_dir
    self.class.fixtures_dir
  end

  def test_projects_url
    self.class.test_projects_url
  end

  def global_vars
    self.class.global_vars
  end

  def report_filename
    self.class.report_filename
  end

  def project_dir
    File.join(fixtures_dir, project_name)
  end

  def report_path
    File.join(project_dir, report_filename)
  end

  def report
    JSON.parse File.read report_path if File.exists? report_path
  end

  def fetch_project
    project_git_url = "#{test_projects_url}/#{project_name}.git"
    unless File.exists?(project_dir)
      %x{git -C #{fixtures_dir} clone #{project_git_url}}
    end
  end

  def reset_branch
    ref = "origin/#{branch}"
    %x{git -C #{project_dir} reset --hard #{ref}}
  end

  def remove_report
    FileUtils.rm report_path if File.exists? report_path
  end

  def docker_run
    var_list = global_vars.merge(variables).map{|k,v| "--env #{k}=#{v}"}.join(" ")
    %x{docker run -t --rm -v #{project_dir}:/app #{var_list} -w /app #{image_name}}
    return $?.to_i
  end
end
