module gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/sirupsen/logrus v1.7.0
	github.com/spiegel-im-spiegel/go-cvss v0.4.0
	github.com/stretchr/testify v1.7.0
	github.com/umisama/go-cvss v0.0.0-20150430082624-a4ad666ead9b
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.1.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.23.0
	gitlab.com/gitlab-org/security-products/analyzers/report/v2 v2.1.0
	gonum.org/v1/gonum v0.8.1
	gopkg.in/yaml.v2 v2.3.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

go 1.13
