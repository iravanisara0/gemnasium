package piplock

import (
	"bytes"
	"encoding/json"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestPipLock(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			// Load fixture
			fixture, err := os.Open("fixtures/wrong_version/Pipfile.lock")
			require.NoError(t, err, "Can't open fixture file")
			defer fixture.Close()
			_, _, err = Parse(fixture)
			require.EqualError(t, err, parser.ErrWrongFileFormat.Error())
		})

		for _, tc := range []string{"simple", "big", "old"} {
			t.Run(tc, func(t *testing.T) {
				fixturesPath := filepath.Join("fixtures", tc, "Pipfile.lock")
				expectationPath := filepath.Join("expect", tc, "packages.json")

				// Load fixture
				fixture, err := os.Open(fixturesPath)
				require.NoError(t, err, "Can't open fixture file")
				defer fixture.Close()

				// Parse & sort
				got, _, err := Parse(fixture)
				require.NoError(t, err)

				// Look for expected output
				if _, err := os.Stat(expectationPath); err == nil {
					// Load expected output
					expect, err := os.Open(expectationPath)
					require.NoError(t, err, "Can't open expect file")
					defer expect.Close()
					var want []parser.Package
					err = json.NewDecoder(expect).Decode(&want)
					require.NoError(t, err)

					require.ElementsMatch(t, want, got)
				} else {
					// Make test fail
					t.Errorf("Creating expectation file: %s", expectationPath)

					// Create target directory if needed
					err := os.MkdirAll(filepath.Dir(expectationPath), 0755)
					require.NoError(t, err, "Cannot create dir: %s")

					// Create missing file
					f, err := os.OpenFile(expectationPath, os.O_CREATE|os.O_WRONLY, 0644)
					require.NoError(t, err, "Cannot create expectation file: %s")

					defer f.Close()
					b, err := json.Marshal(got)
					require.NoError(t, err)
					var out bytes.Buffer
					json.Indent(&out, b, "", "  ")
					out.WriteTo(f)
				}
			})
		}
	})
}
