package remediate

import (
	"context"
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"syscall"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

const (
	nameRequirementsFile = "package.json"
	nameLockFile         = "yarn.lock"
)

var errAffectionNotCured = errors.New("Affection not cured")

// cureYarnLock cures a yarn.lock file.
func cureYarnLock(ctx context.Context, source scanner.File) ([]Cure, error) {

	// get cure function of this source file and with cache
	var cureAffection = newCureYarnAffection(ctx, source)

	// iterate on source affections
	cures := []Cure{}
	for _, a := range source.Affections {
		advID := a.Advisory.Identifier
		log.Debugf("starting remediation of %s", advID)
		cure, err := cureAffection(a, a.Dependency.Name)

		if isKilledErr(err) {
			return cures, context.DeadlineExceeded
		}

		switch err {
		case nil:
			log.Infof("remediation found for %s", advID)
			cures = append(cures, *cure)
		case context.DeadlineExceeded:
			log.Warnf("timeout during remediation of %s", advID)
			return cures, context.DeadlineExceeded
		case errAffectionNotCured:
			log.Debugf("no remediation for %s", advID)
			// ignore
		default:
			return nil, err
		}
	}
	return cures, nil
}

// CureFunc is the type of a function that cures an affection with an upgrade argument.
type CureFunc func(aff scanner.Affection, upgradeArg string) (*Cure, error)

// newCureYarnAffection generates a function that upgrades an affection in the context of a cure.
// It performs the depenencies upgrades using the given upgrade function.
func newCureYarnAffection(ctx context.Context, source scanner.File) CureFunc {
	// create an upgrade function for given source file and context, add chaching
	var upgradeWithCache = newCachedUpgrade(func(arg string) (*UpgradeResult, error) {
		projectDir := filepath.Dir(source.AbsPath())
		return yarnUpgrade(ctx, projectDir, arg)
	})

	return func(aff scanner.Affection, upgradeArg string) (*Cure, error) {
		result, err := upgradeWithCache(upgradeArg)
		if err == errNoopUpgrade {
			return nil, errAffectionNotCured // no change thus not cured
		}
		if err != nil {
			return nil, err
		}

		// find updated dependency matching affected one
		if updated, found := findDep(aff.Dependency, result.Dependencies); !found {
			// dependency is gone so it's cured, proceed
		} else {
			match, err := isVersionInRange(updated.Version, aff.Advisory.AffectedRange)
			if err != nil {
				return nil, err
			}
			if match {
				return nil, errAffectionNotCured // not cured
			}
			// dependency is no longer affected, proceed
		}

		return &Cure{
			Summary:    "Upgrade " + upgradeArg,
			Diff:       result.Diff,
			Affections: []scanner.Affection{aff},
		}, nil
	}
}

// UpgradeFunc is the type of a function that upgrade dependencies using given upgrade argument.
type UpgradeFunc func(upgradeArg string) (*UpgradeResult, error)

// newCachedUpgrade adds caching to an upgrade function.
func newCachedUpgrade(upgrade UpgradeFunc) UpgradeFunc {

	// create a cache
	type cacheEntry struct {
		Result *UpgradeResult
		Error  error
	}
	var cache = make(map[string]cacheEntry)

	// wrap upgrade function and adds caching
	return func(arg string) (*UpgradeResult, error) {
		if entry, ok := cache[arg]; ok {
			return entry.Result, entry.Error
		}
		r, err := upgrade(arg)
		cache[arg] = cacheEntry{Result: r, Error: err}
		return r, err
	}
}

// UpgradeResult combines a yarn upgrade command with the resulting dependencies and git diff.
type UpgradeResult struct {
	Diff         []byte           // Diff is the git diff of the upgraded dependency files.
	Dependencies []parser.Package // Dependencies are the dependencies parsed in the upgraded dependency files.
}

var errNoopUpgrade = errors.New("Upgrade results in no change")

// yarnUpgrade upgrades a yarn project using the "yarn upgrade" command.
// If the lock file is changed, then it returns the diff for the dependency files of the yarn project
// (both the lock and requirements files), as well as the dependencies found in the new lock file.
// The dependency files are restored when the function returns.
func yarnUpgrade(ctx context.Context, dir string, yarnUpgradeArg string) (*UpgradeResult, error) {
	result := &UpgradeResult{}

	// createCmd creates a command where the directory,
	// the environment variables, and the context are set
	var createCmd = func(name string, args ...string) *exec.Cmd {
		var c = exec.CommandContext(ctx, name, args...)
		c.Dir = dir
		c.Env = os.Environ()
		return c
	}

	// restore lock file and requirements file
	defer func() {
		cmd := createCmd("git", "checkout", nameLockFile, nameRequirementsFile)
		output, err := cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), output)
		if err != nil {
			log.Errorf("%s\n%s", cmd.String(), output)
			log.Error("cannot restore files after running upgrade command")
		}
	}()

	// upgrade yarn project
	cmd := createCmd("yarn", "upgrade", "--ignore-engines", yarnUpgradeArg)
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Errorf("%s\n%s", cmd.String(), output)
		return nil, err
	}
	log.Debugf("%s\n%s", cmd.String(), output)

	// skip if the lock file hasn't changed, even if the requirements file has changed;
	// this is because the package manager might reformat the requirements file
	// but ultimately no vulnerability can be fixed if the lock file hasn't changed
	// see https://gitlab.com/gitlab-org/gitlab/-/issues/220487
	cmd = createCmd("git", "diff", "-s", "--exit-code", nameLockFile)
	output, err = cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err == nil {
		return nil, errNoopUpgrade
	}

	// get diff for lock file and requirements file
	cmd = createCmd("git", "diff", nameLockFile, nameRequirementsFile)
	cmd.Stderr = os.Stderr
	diff, err := cmd.Output()
	if err != nil {
		log.Errorf("%s\n%s", cmd.String(), output)
		return nil, err
	}
	log.Debugf("%s\n%s", cmd.String(), diff)
	result.Diff = diff

	// get dependencies for lock file
	pathLockFile := filepath.Join(dir, nameLockFile)
	log.Debugf("parsing lock file: %s", pathLockFile)
	f, err := os.Open(pathLockFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	deps, _, err := yarn.Parse(f)
	if err != nil {
		return nil, err
	}
	result.Dependencies = deps

	return result, nil
}

// isKilledErr tells whether the command returning the given error has been killed.
func isKilledErr(err error) bool {
	if e, ok := err.(*exec.ExitError); ok {
		if e.ProcessState.Sys().(syscall.WaitStatus).Signal() == syscall.SIGKILL {
			return true
		}
	}
	return false
}

// findDep finds a project dependency in a list of packages
func findDep(p parser.Package, pp []parser.Package) (*parser.Package, bool) {
	for _, x := range pp {
		if p.Name == x.Name {
			return &x, true
		}
	}
	return nil, false
}

// isVersionInRange tells whether version is included in requirement.
func isVersionInRange(version, requirement string) (bool, error) {
	query := vrange.Query{Version: version, Range: requirement}
	resolver, err := vrange.NewResolver("npm")
	if err != nil {
		return false, err
	}
	result, err := resolver.Resolve([]vrange.Query{query})
	if err != nil {
		return false, err
	}
	return result.Satisfies(query)
}
