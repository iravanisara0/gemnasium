package poetry

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestNpm(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		for _, tc := range []string{"missing_content_hash", "missing_python_versions"} {
			t.Run(tc, func(t *testing.T) {
				// Load fixture
				fixture, err := os.Open("fixtures/" + tc + "/poetry.lock")
				require.NoError(t, err, "Can't open fixture file")
				defer fixture.Close()
				_, _, err = Parse(fixture)
				require.EqualError(t, err, parser.ErrWrongFileFormatVersion.Error())
			})
		}

		for _, tc := range []string{"simple", "big"} {
			t.Run(tc, func(t *testing.T) {
				// Load fixture
				fixture, err := os.Open("fixtures/" + tc + "/poetry.lock")
				require.NoError(t, err, "Can't open fixture file")
				defer fixture.Close()
				got, _, err := Parse(fixture)
				require.NoError(t, err)

				// Load expected output
				expect, err := os.Open("expect/" + tc + "/packages.json")
				require.NoError(t, err, "Can't open expect file")
				defer expect.Close()
				var want []parser.Package
				err = json.NewDecoder(expect).Decode(&want)
				require.NoError(t, err)

				require.ElementsMatch(t, want, got)
			})
		}
	})
}
